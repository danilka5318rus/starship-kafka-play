# Starship pet project🚀

Kafka based project of mine
Danil Yusupov👨🏽‍💻

## Getting started

- run docker-compose with file in the root of the project
- run kafka-server
- run kafka-tester

## Kafka-UI

There is also Kafka UI available (runing in docker-compose) under `localhost:8082`
