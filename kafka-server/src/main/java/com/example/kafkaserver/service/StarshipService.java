package com.example.kafkaserver.service;

import com.example.kafkaserver.model.StarshipDto;

public interface StarshipService {

    StarshipDto save(StarshipDto dto);

    void send(StarshipDto dto);

    void consume(StarshipDto dto);

}
